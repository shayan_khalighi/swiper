import style from "../styles/Home.module.css";
import Swiper from '../component/SwipX'

import React, { useState } from "react";


export default function Home() {
  
  return (
    <section className={style.main}>
      <div className={style.listContainer}>
        {[...Array(200)].map((e,i) => {
          return(
            <Swiper key={i}  />
         
          )
        })}
      </div>
    </section>
  );
}
