import { useSpring, animated , useSprings} from "@react-spring/web";
import { useDrag, useGesture } from "@use-gesture/react";
import style from "./swiperX.module.css";
import React, { useState } from "react";

export default function SwipX() {
  const [drag, setDrag] = useState(false);

  const [num, setNum] = useState(0);
  const dragEnd = () => {
    setNum(num + 1);
  };

  const [props, api] = useSpring(() => ({ x: 0 }));

  const bind = useGesture({
    onDrag: ({ down, movement: [mx] }) => {
      if (mx < 0) {
        api.start({ x: down ? mx : 0 }), { axis: "x" };
        setDrag(true);
      }
    },
    onDragEnd: ({ movement: [mx] }) => {
      if (mx < -280) dragEnd();
    },
  });

  function handleClick(e) {
    if (drag) {
      console.log(drag);

      setDrag(false);
    }
  }

  return (
    <div className={style.listItemContainer}>
      <div className={style.listItem} onClick={() => handleClick()}>
        {/* <animated.div className={style.listContent} {...bind()} style={props,{touchAction:"pan-y"}} /> */}
        <TouchActionExample  />
      </div>
    </div>
  );
}

function TouchActionExample() {
  const [springs, api] = useSprings(1,() => ({ x: 0 }))

  const bind = useDrag(
    ({ down, movement: [x], args: [index] }) => {if (x < 0) {api.start((i) => i === index && { x: down ? x : 0 }),
    {
      axis: 'x'
    }
  }}
  )


  return (springs.map(({ x }, i) => <animated.div className={style.listContent} {...bind(i)} style={{ x, touchAction: 'pan-y' }} />))
}
